require 'test_helper'

class RetailersControllerTest < ActionController::TestCase
  test "should get by_region" do
    get :by_region
    assert_response :success
  end

  test "should get all" do
    get :all
    assert_response :success
  end

  test "should get show" do
    get :show
    assert_response :success
  end

end
