Xcompany::Application.routes.draw do
  root :to => 'pages#home'


  match "/designers/all", :to => 'designers#all'
  match "/designers/awards", :to => 'designers#awards'
  match "/designers/show", :to => 'designers#show'
  match "/designers/new", :to => 'designers#new'
  match '/designers/:id' => 'designers#show'
  match "/designers/" => 'designers#create'

  resources :designers

  match "/products/new_home" => 'products#new_home'
  match 'products/new_office' => 'products#home_office'
  match "/products/all_home", :to => 'products#all_home'
  match "/products/all_office", :to => 'products#all_office'
  match "/products/by_partners", :to => 'products#by_partners'
  match '/products/office_by_type/:category' => 'products#office_by_type'
  match '/products/office_by_type' => 'products#office_by_type'
  match '/products/home_by_type/:category' => 'products#home_by_type'
  match '/products/home_by_type' => 'products#home_by_type'
  match 'products/products_retailers' => 'products#products_retailers'
  match 'products/show' => 'products#show'
  match 'products/top' => 'products#top'
  match '/products/:id' => 'products#show'
  match "/products/" => 'products#create'

  resources :products

  match "/events/by_date", :to => 'events#by_date'
  match 'events/by_region/', :to => 'events#by_region'
  match "/events/events_products", :to => 'events#events_products'
  match "/events/events_retailers", :to => 'events#events_retailers'
  match "/events/show", :to => 'events#show'
  match "/events/new", :to => 'events#new'
  match '/events/:id' => 'events#show'
  match "/events/" => 'events#create'

  resources :events

  match "/retailers/all", :to => 'retailers#all'
  match 'retailers/by_region/:region', :to => 'retailers#by_region'
  match "/retailers/regions", :to => 'retailers#regions'
  match "/retailers/retailers_products", :to => 'retailers#retailers_products'
  match "/retailers/show", :to => 'retailers#show'
  match "/retailers/where_we_are", :to => 'retailers#where_we_are'
  match "/retailers/new", :to => 'retailers#new'
  match '/retailers/:id' => 'retailers#show'
  match "/retailers/" => 'retailers#create'

  resources :retailers

  match "/admin" => 'pages#admin'
  get "pages/new_product_designer_relation"
  get "pages/new_product_retailer_relation"
  get "pages/new_product_event_relation"
  get "pages/new_retailer_event_relation"
  get "pages/home"
  get "pages/admin"
  get "pages/who_we_are"
  get "pages/where_we_are"
  get "pages/contact_us"
  get "pages/work_with_us"
  get "pages/partners"
  get "pages/services"


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
   match ':controller(/:action(/:id))(.:format)'
end
