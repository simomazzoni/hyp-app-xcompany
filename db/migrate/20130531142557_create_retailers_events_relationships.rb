class CreateRetailersEventsRelationships < ActiveRecord::Migration
  def change
    create_table :retailers_events_relationships do |t|
      t.integer :retailer_id
      t.integer :event_id

      t.timestamps
    end

    add_index :retailers_events_relationships, :retailer_id
    add_index :retailers_events_relationships, :event_id
    add_index :retailers_events_relationships, [:retailer_id, :event_id], unique: true, :name => 'index_retailers_events'

  end
end
