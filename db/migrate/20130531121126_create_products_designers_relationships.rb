class CreateProductsDesignersRelationships < ActiveRecord::Migration
  def change
    create_table :products_designers_relationships do |t|
      t.integer :product_id
      t.integer :designer_id

      t.timestamps
    end

    add_index :products_designers_relationships, :product_id
    add_index :products_designers_relationships, :designer_id
    #add_index :products_designers_relationships, [:product_id, :designer_id], unique: true
  end
end
