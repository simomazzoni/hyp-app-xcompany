class Picture < ActiveRecord::Base
  attr_accessible :url_image, :product_id

  belongs_to :product, class_name: "Product"

end
