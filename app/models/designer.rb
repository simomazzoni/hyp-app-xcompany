class Designer < ActiveRecord::Base
  attr_accessible :awards, :biography, :birthday, :lastname, :name, :photo_url

  validates :name, :presence => true
  validates :lastname, :presence => true
  validates :biography, :presence => true
  validates :awards, :presence => true

  has_many :reverse_products_designers_relationships, foreign_key: "designer_id", class_name:  "ProductsDesignersRelationship", dependent:   :destroy
  has_many :designed_products, through: :reverse_products_designers_relationships, source: :product

  def to_s
    "#{name} #{lastname}"
  end
end
