class Retailer < ActiveRecord::Base
  attr_accessible :address, :description, :mail, :name, :phone_number, :photo_url, :region

  has_many :reverse_products_retailers_relationships, foreign_key: "retailer_id", class_name:  "ProductsRetailersRelationship", dependent:   :destroy
  has_many :products, through: :reverse_products_retailers_relationships, source: :product

  has_many :retailers_events_relationships, foreign_key: "retailer_id", dependent: :destroy
  has_many :events, through: :retailers_events_relationships, source: :event

  validates :name, :presence => true
  validates :address, :presence => true
  validates :description, :presence => true
  validates :mail, :presence => true
  validates :phone_number, :presence => true
  validates :region, :presence => true

end
