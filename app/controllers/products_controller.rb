class ProductsController < ApplicationController
  def office_by_type
    @office_prod = Product.where('partner = "f" AND home_office = "f" AND category = ?', params[:category])
    @cat = params[:category]
  end

  def home_by_type
    @home_prod = Product.where('partner = "f" AND home_office = "t" AND category = ?', params[:category])
    @cat = params[:category]
  end

  def by_partners
    @partners_prod = Product.where('partner = "t"')
  end

  def top
    @top_prod = Product.where('top = "t"')
  end

  def show
    @product = Product.find(params[:id])

    @pro = params[:pro]
    @des = params[:des]
    @hom = params[:hom]
    @off = params[:off]
    @top = params[:top]
    @par = params[:par]
    @ret = params[:ret]
    @eve = params[:eve]
    @reg = params[:reg]
    @dat = params[:dat]
    @rall = params[:rall]

    if @pro and @pro != '0'
      @t = Product.find(@pro)
      @proname = @t.name
    end

    if @des
      @t = Designer.find(@des)
      @desname = @t.name + @t.lastname
    end

    if @ret
      @t = Retailer.find(@ret)
      @retname = @t.name
    end

    if @eve
      @t = Event.find(@eve)
      @evename = @t.name
    end

    @path_left = case
                   when @des
                     @p = Designer.find(@des).designed_products
                     hash = Hash[@p.map.with_index.to_a]
                     @i = hash[@product]
                     @m = @p.count

                     (@i == 0 ? products_show_path(:id => @p[@m - 1].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : products_show_path(:id => @p[@i.to_i - 1].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                   when (@hom and @product.category == @hom)
                     @p = Product.where('category = ?', @hom)
                     hash = Hash[@p.map.with_index.to_a]
                     @i = hash[@product]
                     @m = @p.count

                     (@i == 0 ? products_show_path(:id => @p[@m - 1].id, :pro => @pro, :hom => @hom, :des => @des, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : products_show_path(:id => @p[@i.to_i - 1].id, :pro => @pro, :hom => @hom, :des => @des, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                   when (@off and @product.category == @off)
                     @p = Product.where('category = ?', @off)
                     hash = Hash[@p.map.with_index.to_a]
                     @i = hash[@product]
                     @m = @p.count

                     (@i == 0 ? products_show_path(:id => @p[@m - 1].id, :pro => @pro, :off => @off, :des => @des, :hom => @hom, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : products_show_path(:id => @p[@i.to_i - 1].id, :pro => @pro, :off => @off, :des => @des, :hom => @hom, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                   when (@top and @product.top == true)
                     @p = Product.where('top = "t"')
                     hash = Hash[@p.map.with_index.to_a]
                     @i = hash[@product]
                     @m = @p.count

                     (@i == 0 ? products_show_path(:id => @p[@m - 1].id, :pro => @pro, :top => @top, :des => @des, :hom => @hom, :off => @off, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : products_show_path(:id => @p[@i.to_i - 1].id, :pro => @pro, :top => @top, :des => @des, :hom => @hom, :off => @off, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                   when (@par and @product.partner == true)
                     @p = Product.where('partner = "t"')
                     @hash = Hash[@p.map.with_index.to_a]
                     @i = @hash[@product]
                     @m = @p.count

                     (@i == 0 ? products_show_path(:id => @p[@m - 1].id, :pro => @pro, :par => @par, :des => @des, :hom => @hom, :off => @off, :top => @top, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : products_show_path(:id => @p[@i.to_i - 1].id, :pro => @pro, :par => @par, :des => @des, :hom => @hom, :off => @off, :top => @top, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                   when @ret
                     @p = Retailer.find(@ret).products
                     @hash = Hash[@p.map.with_index.to_a]
                     @i = @hash[@product]
                     @m = @p.count

                     (@i == 0 ? products_show_path(:id => @p[@m - 1].id, :pro => @pro, :ret => @ret, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : products_show_path(:id => @p[@i.to_i - 1].id, :pro => @pro, :ret => @ret, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                   when @eve
                     @p = Event.find(@eve).products
                     @hash = Hash[@p.map.with_index.to_a]
                     @i = @hash[@product]
                     @m = @p.count

                     (@i == 0 ? products_show_path(:id => @p[@m - 1].id, :pro => @pro, :eve => @eve, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :reg => @reg, :dat => @dat, :rall => @rall) : products_show_path(:id => @p[@i.to_i - 1].id, :pro => @pro, :eve => @eve, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :reg => @reg, :dat => @dat, :rall => @rall))
                   else
                 end


    @path_up = case
                  when @des
                    designers_show_path(:id => @des, :pro => 0, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall)
                  when (@hom and @product.category == @hom)
                    products_home_by_type_path(:category => @hom, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall)
                  when (@off and @product.category == @off)
                    products_office_by_type_path(:category => @off, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall)
                  when (@top and @product.top == true)
                    products_top_path
                  when (@par and @product.partner == true)
                    products_by_partners_path
                  when @ret
                    retailers_retailers_products_path(:id => @ret, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall)
                  when @eve
                    events_events_products_path(:id => @eve, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall)
                  else
                end

    @path_right = case
                   when @des
                     @p = Designer.find(@des).designed_products
                     hash = Hash[@p.map.with_index.to_a]
                     @i = hash[@product]
                     @m = @p.count

                     (@i == (@m - 1) ? products_show_path(:id => @p[0].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : products_show_path(:id => @p[@i.to_i + 1].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                   when (@hom and @product.category == @hom)
                     @p = Product.where('category = ?', @hom)
                     hash = Hash[@p.map.with_index.to_a]
                     @i = hash[@product]
                     @m = @p.count

                     (@i == (@m - 1) ? products_show_path(:id => @p[0].id, :pro => @pro, :hom => @hom, :des => @des, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : products_show_path(:id => @p[@i.to_i + 1].id, :pro => @pro, :hom => @hom, :des => @des, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                   when (@off and @product.category == @off)
                     @p = Product.where('category = ?', @off)
                     hash = Hash[@p.map.with_index.to_a]
                     @i = hash[@product]
                     @m = @p.count

                     (@i == (@m - 1) ? products_show_path(:id => @p[0].id, :pro => @pro, :off => @off, :des => @des, :hom => @hom, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : products_show_path(:id => @p[@i.to_i + 1].id, :pro => @pro, :off => @off, :des => @des, :hom => @hom, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                   when (@top and @product.top == true)
                     @p = Product.where('top = "t"')
                     hash = Hash[@p.map.with_index.to_a]
                     @i = hash[@product]
                     @m = @p.count

                     (@i == (@m - 1) ? products_show_path(:id => @p[0].id, :pro => @pro, :top => @top, :des => @des, :hom => @hom, :off => @off, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : products_show_path(:id => @p[@i.to_i + 1].id, :pro => @pro, :top => @top, :des => @des, :hom => @hom, :off => @off, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                    when (@par and @product.partner == true)
                     @p = Product.where('partner = "t"')
                     @hash = Hash[@p.map.with_index.to_a]
                     @i = @hash[@product]
                     @m = @p.count

                     (@i == (@m - 1) ? products_show_path(:id => @p[0].id, :pro => @pro, :par => @par, :des => @des, :hom => @hom, :off => @off, :top => @top, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : products_show_path(:id => @p[@i.to_i + 1].id, :pro => @pro, :par => @par, :des => @des, :hom => @hom, :off => @off, :top => @top, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                   when @ret
                     @p = Retailer.find(@ret).products
                     @hash = Hash[@p.map.with_index.to_a]
                     @i = @hash[@product]
                     @m = @p.count

                     (@i == (@m - 1) ? products_show_path(:id => @p[0].id, :pro => @pro, :ret => @ret, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : products_show_path(:id => @p[@i.to_i + 1].id, :pro => @pro, :ret => @ret, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                   when @eve
                     @p = Event.find(@eve).products
                     @hash = Hash[@p.map.with_index.to_a]
                     @i = @hash[@product]
                     @m = @p.count

                     (@i == (@m - 1) ? products_show_path(:id => @p[0].id, :pro => @pro, :eve => @eve, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :reg => @reg, :dat => @dat, :rall => @rall) : products_show_path(:id => @p[@i.to_i + 1].id, :pro => @pro, :eve => @eve, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :reg => @reg, :dat => @dat, :rall => @rall))
                   else
                 end

  end

  def products_retailers
    @product = Product.find(params[:id])

    @pro = params[:pro]
    @des = params[:des]
    @hom = params[:hom]
    @off = params[:off]
    @top = params[:top]
    @par = params[:par]
    @ret = params[:ret]
    @eve = params[:eve]
    @reg = params[:reg]
    @dat = params[:dat]
    @rall = params[:rall]
  end

  def all_office
  end

  def all_home
  end

  def new_home
    @product = Product.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @product }
    end
  end

  def create
    @product = Product.new(params[:product])

    @product.home_office = "t"
    @product.partner = "f"

    respond_to do |format|
      if @product.save
        format.html { redirect_to pages_admin_path, :notice => 'Product was successfully created.' }
      else
        format.html { render :action => "new_home" }
      end
    end

  end
end
