class RetailersController < ApplicationController
  def by_region
    @retailers_by_region = Retailer.where('region = ?', params[:region])
    @reg = params[:region]
  end

  def all
    @all_retailers = Retailer.all
  end

  def show
    @retailer = Retailer.find(params[:id])

    @pro = params[:pro]
    @des = params[:des]
    @hom = params[:hom]
    @off = params[:off]
    @top = params[:top]
    @par = params[:par]
    @ret = params[:ret]
    @eve = params[:eve]
    @reg = params[:reg]
    @dat = params[:dat]
    @rall = params[:rall]

    if !@ret and !@rall and !@pro and !@eve
      @ret = @retailer.region
    end

    @path_left = case
                   when @pro
                     @r = Product.find(@pro).retailers
                     hash = Hash[@r.map.with_index.to_a]
                     @i = hash[@retailer]
                     @m = @r.count

                     (@i == 0 ? retailers_show_path(:id => @r[@m - 1].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : retailers_show_path(:id => @r[@i.to_i - 1].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                   when @ret
                     @r = Retailer.where('region = ?', @ret)
                     hash = Hash[@r.map.with_index.to_a]
                     @i = hash[@retailer]
                     @m = @r.count

                     (@i == 0 ? retailers_show_path(:id => @r[@m - 1].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : retailers_show_path(:id => @r[@i.to_i - 1].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                   when @eve
                     @r = Event.find(@eve).retailers
                     hash = Hash[@r.map.with_index.to_a]
                     @i = hash[@retailer]
                     @m = @r.count

                     (@i == 0 ? retailers_show_path(:id => @r[@m - 1].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : retailers_show_path(:id => @r[@i.to_i - 1].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                   else
                 end

    @path_up = case
                    when @pro
                      products_products_retailers_path(:id => @pro, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat)
                    when @ret
                      "/retailers/by_region/" + @ret
                    when @eve
                      events_events_retailers_path(:id => @eve, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat)
                    else
                      retailers_all_path
                  end

    @path_right = case
                   when @pro
                     @r = Product.find(@pro).retailers
                     hash = Hash[@r.map.with_index.to_a]
                     @i = hash[@retailer]
                     @m = @r.count

                     (@i == (@m - 1) ? retailers_show_path(:id => @r[0].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : retailers_show_path(:id => @r[@i.to_i + 1].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                    when @ret
                      @r = Retailer.where('region = ?', @ret)
                      hash = Hash[@r.map.with_index.to_a]
                      @i = hash[@retailer]
                      @m = @r.count

                      (@i == (@m - 1) ? retailers_show_path(:id => @r[0].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : retailers_show_path(:id => @r[@i.to_i + 1].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                    when @eve
                      @r = Event.find(@eve).retailers
                      hash = Hash[@r.map.with_index.to_a]
                      @i = hash[@retailer]
                      @m = @r.count

                      (@i == (@m - 1) ? retailers_show_path(:id => @r[0].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : retailers_show_path(:id => @r[@i.to_i + 1].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                    else
                  end
  end

  def retailers_products
    @retailer = Retailer.find(params[:id])

    @pro = params[:pro]
    @des = params[:des]
    @hom = params[:hom]
    @off = params[:off]
    @top = params[:top]
    @par = params[:par]
    @ret = params[:ret]
    @eve = params[:eve]
    @reg = params[:reg]
    @dat = params[:dat]
    @rall = params[:rall]
  end

  def regions
  end

  def where_we_are
    @retailer = Retailer.find(params[:id])

    @pro = params[:pro]
    @des = params[:des]
    @hom = params[:hom]
    @off = params[:off]
    @top = params[:top]
    @par = params[:par]
    @ret = params[:ret]
    @eve = params[:eve]
    @reg = params[:reg]
    @dat = params[:dat]
    @rall = params[:rall]

    if !@ret and !@rall and !@pro and !@eve
      @ret = @retailer.region
    end

    @path_left = case
                   when @pro
                     @r = Product.find(@pro).retailers
                     hash = Hash[@r.map.with_index.to_a]
                     @i = hash[@retailer]
                     @m = @r.count

                     (@i == 0 ? retailers_show_path(:id => @r[@m - 1].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : retailers_show_path(:id => @r[@i.to_i - 1].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                   when @ret
                     @r = Retailer.where('region = ?', @ret)
                     hash = Hash[@r.map.with_index.to_a]
                     @i = hash[@retailer]
                     @m = @r.count

                     (@i == 0 ? retailers_show_path(:id => @r[@m - 1].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : retailers_show_path(:id => @r[@i.to_i - 1].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                   when @eve
                     @r = Event.find(@eve).retailers
                     hash = Hash[@r.map.with_index.to_a]
                     @i = hash[@retailer]
                     @m = @r.count

                     (@i == 0 ? retailers_show_path(:id => @r[@m - 1].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : retailers_show_path(:id => @r[@i.to_i - 1].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                   else
                 end

    @path_up = case
                 when @pro
                   products_products_retailers_path(:id => @pro, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat)
                 when @ret
                   "/retailers/by_region/" + @ret
                 when @eve
                   events_events_retailers_path(:id => @eve, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat)
                 else
                   retailers_all_path
               end

    @path_right = case
                    when @pro
                      @r = Product.find(@pro).retailers
                      hash = Hash[@r.map.with_index.to_a]
                      @i = hash[@retailer]
                      @m = @r.count

                      (@i == (@m - 1) ? retailers_show_path(:id => @r[0].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : retailers_show_path(:id => @r[@i.to_i + 1].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                    when @ret
                      @r = Retailer.where('region = ?', @ret)
                      hash = Hash[@r.map.with_index.to_a]
                      @i = hash[@retailer]
                      @m = @r.count

                      (@i == (@m - 1) ? retailers_show_path(:id => @r[0].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : retailers_show_path(:id => @r[@i.to_i + 1].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                    when @eve
                      @r = Event.find(@eve).retailers
                      hash = Hash[@r.map.with_index.to_a]
                      @i = hash[@retailer]
                      @m = @r.count

                      (@i == (@m - 1) ? retailers_show_path(:id => @r[0].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall) : retailers_show_path(:id => @r[@i.to_i + 1].id, :pro => @pro, :des => @des, :hom => @hom, :off => @off, :top => @top, :par => @par, :ret => @ret, :eve => @eve, :reg => @reg, :dat => @dat, :rall => @rall))
                    else
                  end
end

  def new
    @retailer = Retailer.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @retailer }
    end
  end

  def create
    @retailer = Retailer.new(params[:retailer])

    @retailer.photo_url = "productdefault.jpg"

    respond_to do |format|
      if @retailer.save
        format.html { redirect_to pages_admin_path, :notice => 'Retailer was successfully created.' }
      else
        format.html { render :action => "new" }
      end
    end

  end
end
