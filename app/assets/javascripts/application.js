// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require_tree .

$(document).ready(function() {
    $("#prodotti").mouseenter(function(){$(".rivenditorisubmenu").fadeOut("slow");$(".eventisubmenu").fadeOut("slow");$(".prodottisubmenu").fadeIn("slow")});
    $(".prodottisubmenu").mouseleave(function(){$(".prodottisubmenu").fadeOut("slow")});
    //$("#rivenditori").mouseenter(function(){$(".prodottisubmenu").fadeOut("slow");$(".eventisubmenu").fadeOut("slow");$(".rivenditorisubmenu").fadeIn("slow")});
    //$(".rivenditorisubmenu").mouseleave(function(){$(".rivenditorisubmenu").fadeOut("slow")});
    $("#eventi").mouseenter(function(){console.log("asd");$(".prodottisubmenu").fadeOut("slow");$(".rivenditorisubmenu").fadeOut("slow");$(".eventisubmenu").fadeIn("slow")});
    $(".eventisubmenu").mouseleave(function(){$(".eventisubmenu").fadeOut("slow")});

    $("#arrowleft").click(function(){
        left = parseInt($("#slider").css("left"));
        if(left<=-600){
            $("#slider").animate({
                left: left+600
            },1500);
        }else{
            $("#slider").animate({
                left: 0
            },1500);
        }
    });

    $("#arrowright").click(function(){
        left = parseInt($("#slider").css("left"));
        if(left>-parseInt($("#slider").css("width"))+600){
            $("#slider").animate({
                left: left-=600
            },1500);
        }else{
            $("#slider").animate({
                left: -parseInt($("#slider").css("width"))+600
            },1500);
        }
    });
});

